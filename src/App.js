import React from "react";
import { Switch, Route } from "react-router-dom";
import ListPost from "./components/ListPost/ListPost";
import Post from "./components/Post/Post";

function App() {
  return (
    <Switch>
      <Route exact path="/">
        <ListPost />
      </Route>
      <Route path="/posts/:id">
        <Post />
      </Route>
    </Switch>
  );
}

export default App;
